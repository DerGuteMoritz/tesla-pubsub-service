(ns tesla-pubsub-service.bus-test
  (:require [clojure.test :refer :all]
            [tesla-pubsub-service.system :refer [pubsub-system]]
            [tesla-pubsub-service.bus :as bus]
            [com.stuartsierra.component :as comp]
            [clojure.core.async :as async]))

;; FIXME: de.otto.tesla.util.test-utils is not available via the de.otto/tesla-microservice library.
(defmacro with-started
  "bindings => [name init ...]

  Evaluates body in a try expression with names bound to the values
  of the inits after (.start init) has been called on them. Finally
  a clause calls (.stop name) on each name in reverse order."
  [bindings & body]
  (if (and
       (vector? bindings) "a vector for its binding"
       (even? (count bindings)) "an even number of forms in binding vector")
    (cond
      (= (count bindings) 0) `(do ~@body)
      (symbol? (bindings 0)) `(let [~(bindings 0) (comp/start ~(bindings 1))]
                                (try
                                  (with-started ~(subvec bindings 2) ~@body)
                                  (finally
                                    (comp/stop ~(bindings 0)))))
      :else (throw (IllegalArgumentException.
                    "with-started-system only allows Symbols in bindings")))
    (throw (IllegalArgumentException.
            "not a vector or bindings-count is not even"))))

(deftest ^:unit events-should-be-subscribable
  (with-started [started (pubsub-system {})]
    (let [bus (:pubsub-bus started)
          ch (async/chan 3)]
      (try
        (bus/subscribe bus "foo" ch)
        (bus/subscribe bus "qux" ch)
        (bus/publish bus "foo" :first)
        (bus/publish bus "bar" :second)
        (bus/publish bus "qux" :third)
        (is (= (async/<!! ch) {:topic "foo" :id 1 :value :first}))
        (is (= (async/<!! ch) {:topic "qux" :id 3 :value :third}))
        (is (= (bus/publication-count bus) 3))
        (finally
          (async/close! ch))))))
