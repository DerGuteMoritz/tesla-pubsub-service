(defproject tesla-pubsub-service "0.1.0-SNAPSHOT"
  :description "A rudimentary pubsub tesla microservice"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0-alpha5"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [org.immutant/web "2.0.0-beta2"]
                 [de.otto/tesla-microservice "0.1.7"]]
  :main tesla-pubsub-service.system)
