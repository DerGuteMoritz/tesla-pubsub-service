# tesla-pubsub-service

A pubsub microservice demo based on [tesla-microservice](https://github.com/otto-de/tesla-microservice).

## Usage

### Starting the server

    lein run

This will start the server with the default configuration, i.e. it
will listen on port 8080 and not bind to any specific host.


### Publishing

Publishing an event to a topic:

    POST /events/some-topic HTTP/1.1
    Host: localhost:8080
    Content-Type: application/edn
    Content-Length: 17

    {:some "payload"}

Only `application/edn` is supported right now. The body can be any
datum representable as EDN. It will be forwarded as an event to all
clients currently subscribed to the given topic. If no client is
currently subscribed to the topic, the event will be discarded.

The response status will be `204 No Content` if successful.


### Subscribing

Subscribing to a single topic:

    GET /events/some-topic
    Host: localhost:8080

For subscribing to multiple topics, separate them with commas:

    GET /events/some-topic,another-topic,yet-another-topic
    Host: localhost:8080

The server will respond with a status of `200 OK` and long-lived
streaming response using `Transfer-Encoding: chunked` through which it
will send events as they are published on any of the subscribed
topics.

Events are EDN maps of the following structure:

    {:id 10, :topic "some-topic", :value {:some "payload"}}

* `:id` is an integer uniquely identifying the event
* `:topic` is the topic this event was published to
* `:value` is the payload the publisher sent


## License

Copyright (c) 2015, Moritz Heidkamp
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the
   distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
