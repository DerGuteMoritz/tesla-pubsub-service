(ns tesla-pubsub-service.system
  (:require [de.otto.tesla.system :as system]
            [com.stuartsierra.component :as c]
            [tesla-pubsub-service.server :refer [new-server]]
            [tesla-pubsub-service.handler :refer [new-handler]]
            [tesla-pubsub-service.bus :refer [new-bus]])
  (:gen-class))

(defn pubsub-system [runtime-config]
  (-> (system/empty-system (merge {:name "pubsub-service"} runtime-config))
      (assoc :pubsub-bus
             (c/using (new-bus) [:app-status :metering]))
      (assoc :pubsub-handler
             (c/using (new-handler) [:config :routes :pubsub-bus]))
      (assoc :server
             (c/using (new-server) [:config :routes :pubsub-handler]))))

(defn -main [& args]
  (system/start-system (pubsub-system {})))

(defonce system nil)

(defn start
  ([] (start {}))
  ([runtime-config]
   ;; NOTE: We can't use system/start-system here because it doesn't
   ;; return the started system (which is a bug)
   (alter-var-root #'system (constantly (c/start (pubsub-system runtime-config))))))

(defn stop []
  (alter-var-root #'system system/stop))
