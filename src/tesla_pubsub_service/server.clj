(ns tesla-pubsub-service.server
  (:require [com.stuartsierra.component :as component]
            [compojure.route :as route]
            [compojure.core :as compojure]
            [immutant.web :as immutant]
            [de.otto.tesla.stateful.routes :as rts]
            [clojure.tools.logging :as log]))

(defrecord Server [config routes server]
  component/Lifecycle
  (start [self]
    (log/info "-> starting server")
    (let [port (Integer. (get-in config [:config :server-port]))
          all-routes (compojure/routes (rts/routes routes)
                                       (route/not-found "Not found"))
          server (immutant/run all-routes :port port)]
      (assoc self :server server)))

  (stop [self]
    (log/info "<- stopping server")
    (immutant/stop server)
    (assoc self :server nil)))

(defn new-server []
  (map->Server {}))
