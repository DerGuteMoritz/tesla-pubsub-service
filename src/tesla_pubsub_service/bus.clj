(ns tesla-pubsub-service.bus
  (:require [de.otto.tesla.stateful.metering :as metering]
            [de.otto.tesla.stateful.app-status :as app-status]
            [de.otto.status :as status]
            [metrics.timers :as timers]
            [com.stuartsierra.component :as c]
            [clojure.tools.logging :as log]
            [clojure.core.async :as async]))

(defn make-input-chan [pub-count]
  (async/chan 1 (map #(assoc % :id (swap! pub-count inc)))))

(defn current-status [pub-count]
  (let [cnt @pub-count]
    (status/status-detail :pubsub-bus :ok (str cnt " events published") {:pub-count cnt})))

(defprotocol PubSub
  (subscribe [self topic ch])
  (unsubscribe [self topic ch])
  (publish [self topic value])
  (publication-count [self]))

(defrecord Bus [input pub pub-count timer]
  c/Lifecycle
  (start [self]
    (log/info "-> starting pubsub bus")
    (let [pub-count (atom 0)
          input (make-input-chan pub-count)
          pub (async/pub input :topic)
          ;; FIXME: There seems to be no API for de-registering a
          ;; timer so we keep it around and re-use it.
          timer (or timer (metering/timer! (:metering self) "event publications"))]
      (app-status/register-status-fun (:app-status self) (partial current-status pub-count))
      (assoc self
             :pub-count pub-count
             :timer timer
             :input input
             :pub pub)))
  (stop [self]
    (log/info "<- stopping pubsub bus")
    (async/close! input)
    (assoc self
           :input nil
           :pub nil?
           :pub-count nil))
  
  PubSub
  (subscribe [self topic ch]
    (async/sub pub topic ch))
  (unsubscribe [self topic ch]
    (async/unsub pub topic ch))
  (publish [self topic value]
    {:pre [(string? topic)]}
    (let [event {:topic topic :value value}]
      (timers/time! timer (async/>!! input event))))
  (publication-count [self] @pub-count))

(defn new-bus []
  (map->Bus {}))
