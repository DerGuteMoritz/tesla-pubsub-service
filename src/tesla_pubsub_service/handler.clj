(ns tesla-pubsub-service.handler
  (:require [de.otto.tesla.stateful.routes :as routes]
            [tesla-pubsub-service.bus :as bus]
            [com.stuartsierra.component :as c]
            [clojure.tools.logging :as log]
            [compojure.core :as compojure :refer [GET POST]]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [clojure.core.async :as async]
            [clojure.string :as s]
            [immutant.web.async :as immutant-async])
  (:import java.io.PushbackReader))

(defn edn-read [input-stream]
  (with-open [is input-stream
              rdr (io/reader is)
              pbrdr (PushbackReader. rdr)]
    (edn/read pbrdr)))

(defn publish-event [system req]
  (let [value (edn-read (:body req))
        topic (get-in req [:params :topic])
        bus (:pubsub-bus system)]
    (bus/publish bus topic value)
    {:status 204}))

(defn subscribe-events [system req]
  (let [bus (:pubsub-bus system)
        topics (-> (get-in req [:params :topics])
                   (str)
                   (s/trim)
                   (s/split #"\s*,\s*"))]
    (if (seq topics)
      (let [make-buf (or (get-in system [:config :config :pubsub-consumer-buffer-fn])
                         #(async/sliding-buffer 10))
            in (async/chan (make-buf))
            callbacks {:on-open (fn [ch]
                                  (doseq [topic topics]
                                    (bus/subscribe bus topic in))
                                  (async/go-loop []
                                    (if-let [event (async/<! in)]
                                      (when (immutant-async/send! ch (prn-str event))
                                        (recur))
                                      (immutant-async/close ch))))
                       :on-error (fn [ch exn]
                                   (log/error (str exn))
                                   (immutant-async/close ch))
                       :on-close (fn [ch _]
                                   (async/close! in))}
            resp (immutant-async/as-channel req callbacks)]
        (assoc-in resp [:headers "content-type"] "application/edn"))
      {:status 400
       :body "Must give at least one topic"})))

(defn make-routes [system]
  [(POST "/events/:topic" req
         (publish-event system req))
   (GET ["/events/:topics" :topics #"[a-z0-9,_-]+"] req
        (subscribe-events system req))])

(defrecord Handler []
  c/Lifecycle
  (start [self]
    (log/info "-> starting pubsub handler")
    (routes/register-routes (:routes self) (make-routes self)))
  (stop [self]
    (log/info "<- stopping pubsub handler")
    self))

(defn new-handler []
  (map->Handler {}))
